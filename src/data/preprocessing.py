import click
import pandas as pd
import spacy
from sklearn.model_selection import train_test_split

random_state = 42


def preprocess_data(data: pd.DataFrame) -> pd.DataFrame:
    nlp = spacy.load("en_core_web_sm")

    data['text'] = data['text'].astype(str)
    data['cleaned_text'] = data['text'].apply(
        lambda x: ' '.join(
            token.lemma_.lower() for token in nlp(x) if
            not token.is_stop
            and not token.is_punct
            and not token.is_digit
            and not token.like_email
            and not token.like_num
            and not token.is_space
        )
    )

    return data


@click.command()
@click.argument("input_path", type=click.Path(exists=True))
@click.argument("output_path_x_train", type=click.Path())
@click.argument("output_path_y_train", type=click.Path())
@click.argument("output_path_x_test", type=click.Path())
@click.argument("output_path_y_test", type=click.Path())
def main(input_path, output_path_x_train, output_path_y_train, output_path_x_test, output_path_y_test):
    data = pd.read_csv(input_path, encoding='iso-8859-1')[['email', 'label']].rename(columns={'email': 'text'})
    processed_data = preprocess_data(data)

    X_train, X_test, y_train, y_test = train_test_split(processed_data['cleaned_text'], processed_data['label'],
                                                        random_state=random_state)

    X_train.to_csv(output_path_x_train, index=False)
    y_train.to_csv(output_path_y_train, index=False)
    X_test.to_csv(output_path_x_test, index=False)
    y_test.to_csv(output_path_y_test, index=False)


if __name__ == "__main__":
    main()
