import click
import pandas as pd
import pickle
from sklearn.metrics import classification_report


@click.command()
@click.argument("model_path", type=click.Path(exists=True))
@click.argument("input_path_x_test", type=click.Path(exists=True))
@click.argument("input_path_y_test", type=click.Path(exists=True))
@click.argument("output_path_predictions", type=click.Path())
def predict_and_evaluate(model_path, input_path_x_test, input_path_y_test, output_path_predictions):
    # Загрузка модели
    with open(model_path, 'rb') as f:
        model = pickle.load(f)

    # Загрузка тестовых данных
    X_test = pd.read_csv(input_path_x_test)
    y_test = pd.read_csv(input_path_y_test)
    X_test = X_test.iloc[:, 0].astype(str).tolist()

    # Предсказание
    predictions = model.predict(X_test)

    # Сохранение предсказаний
    pd.DataFrame(predictions, columns=["prediction"]).to_csv(output_path_predictions, index=False)

    # Оценка метрик
    report = classification_report(y_test, predictions, output_dict=True)
    report_df = pd.DataFrame(report).transpose()

    # Вывод отчета
    print(report_df)

    # Сохранение отчета в файл
    report_df.to_csv("metrics_report.csv", index=True)
    print(f"Predictions and metrics saved to {output_path_predictions} and metrics_report.csv")


if __name__ == "__main__":
    predict_and_evaluate()
