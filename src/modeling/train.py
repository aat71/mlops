import click
import os
import pandas as pd
import pickle

from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.model_selection import KFold, train_test_split
from sklearn.pipeline import Pipeline
from sklearn.linear_model import LogisticRegression
from sklearn.experimental import enable_halving_search_cv  # noqa # pylint: disable=unused-import
from sklearn.model_selection import HalvingGridSearchCV
from sklearn.metrics import classification_report

import mlflow
from mlflow.models.signature import infer_signature
from dotenv import load_dotenv

load_dotenv()

remote_server_uri = os.getenv("MLFLOW_TRACKING_URI")
mlflow.set_tracking_uri(remote_server_uri)


@click.command()
@click.argument("input_path_x_train", type=click.Path(exists=True))
@click.argument("input_path_y_train", type=click.Path(exists=True))
@click.argument("output_path_model", type=click.Path())
def main(input_path_x_train, input_path_y_train, output_path_model):
    random_state = 42
    with mlflow.start_run():
        X = pd.read_csv(input_path_x_train)
        y = pd.read_csv(input_path_y_train)
        X = X.iloc[:, 0].astype(str).tolist()

        X_train, X_test, y_train, y_test = train_test_split(X, y,
                                                            test_size=0.25,
                                                            random_state=random_state,
                                                            stratify=y)

        splitter = KFold(n_splits=5, shuffle=True, random_state=random_state)

        pipe_logisticRegression = Pipeline(
            steps=[
                ('tfidf', TfidfVectorizer()),
                ('clf', LogisticRegression(random_state=random_state))
            ]
        )

        logisticRegression_parameter_grid = {
            'clf__penalty': ['l1', 'l2'],
            'clf__solver': ['liblinear', 'saga'],
            'clf__fit_intercept': (False, True),
            'clf__C': [0.01, 0.1, 0.5, 0.7, 1, 10]
        }

        grid_search_logisticRegression = HalvingGridSearchCV(
            pipe_logisticRegression,
            param_grid=logisticRegression_parameter_grid,
            n_jobs=-1,
            verbose=1,
            cv=splitter,
            scoring='f1',
            random_state=random_state,
        )
        grid_search_logisticRegression.fit(X_train, y_train.values.ravel())

        best_params = grid_search_logisticRegression.best_params_

        # Сохраняем лучшую модель
        with open(output_path_model, 'wb') as f:
            pickle.dump(grid_search_logisticRegression.best_estimator_, f)

        # Вычисляем метрики
        model = grid_search_logisticRegression.best_estimator_
        y_pred = model.predict(X_test)
        class_report = classification_report(y_test, y_pred, output_dict=True)
        score = {
            "precision": class_report["1"]["precision"],
            "recall": class_report["1"]["recall"],
            "f1-score": class_report["1"]["f1-score"],
            "support": class_report["1"]["support"],
            "accuracy": class_report["accuracy"],
        }

        signature = infer_signature(X_test, y_pred)

        mlflow.log_params(best_params)
        mlflow.log_metrics(score)
        mlflow.sklearn.log_model(
            sk_model=model,
            artifact_path="model",
            registered_model_name="logreg_old_vers",
            signature=signature
        )

    # Пример использования переменной df
    # experiment = mlflow.get_experiment_by_name("Default")
    # experiment_id = experiment.experiment_id
    # df = mlflow.search_runs([experiment_id])
    # print(df)

    print(best_params)


if __name__ == "__main__":
    main()
